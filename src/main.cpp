#include <Arduino.h>

#include <USBMIDI.h>

// // http : //www.robotoid.com/appnotes/sensors-piezo-disc-touch-bar.html

const int midichannel = 0;


//*******************************************************************************************************************
// Setup
//*******************************************************************************************************************

class Pad
{
  protected:
    int _pin;
    int _padNote;
    bool _active;
    bool _holdActive;
    int _playTime;
    int _maxPlayTime;
    uint32 _maxWaitTime;
    //bool _hiHat;
  public:
    Pad(int pin, int padNote);
    void act();
};

Pad::Pad(int pin,int padNote)
{
  _pin = pin;
  _padNote = padNote;
  _active = false;
  _holdActive = false;
  _playTime = 0;
  _maxPlayTime = 90;
  _maxWaitTime = 500; //wait time for digital pads, 1000 millis
}

const int playClipNote = 37;
//del 38 al 44 son los botones de seleccion de escena

bool isClipButton(int note) {
  if ((note >= 38 )&&(note <=44)){
    return true;
  }else
  {
    return false;
  }
}
uint32 debounceTime = 200;
void Pad::act(){
  if (millis() - _playTime > debounceTime){
    if (!digitalRead(_pin)){
              if (!_active){
                  _playTime = millis();
                  // Serial.print("Boton ");
                  // Serial.print(_pin);
                  // Serial.println(": ON");
                  _active = true;
                  USBMIDI.sendNoteOn(midichannel, _padNote, 127);
                  if(isClipButton(_padNote)){ //TODO alto fix reveer
                  USBMIDI.sendNoteOn(midichannel, playClipNote, 127);
              }
              }
              // read = true;
              if (millis() - _playTime > _maxWaitTime){
                if(!_holdActive)
                {
                  USBMIDI.sendNoteOn(midichannel, _padNote+1, 127);
                  _holdActive = true;
                  }
                  // Serial.print("Boton ");
                  // Serial.print(_pin);
                  // Serial.println(": mantenido apretado");
              } 
          }
          else {
              _active = false;
              _holdActive = false;
              _playTime = millis();
          }
  }
}


class AnaloguePad : public Pad
{
  private:
    int _padCutOff;
  public:
    AnaloguePad(int pin, int padCutOff, int padNote):
    Pad(pin, padNote)
    {
      this->_padCutOff = padCutOff;
    };
    void act();
    void basicAct();
};

class HiHatPad : public AnaloguePad
{
  private:
    bool _hiHat;
    bool _basePadNote;
    void changeNote(int note);
  public:
    HiHatPad(int pin, int padCutOff, int padNote):
    AnaloguePad(pin, padCutOff, padNote)
    {
      _hiHat = true;
      _basePadNote = padNote;
    };
    void act();
    void basicAct();
};

void AnaloguePad::basicAct()
{
  int newValue = 0;
  int lastValue = 0;   
  // int outputValue = 0; 
  int count = 0;
  int maxCount = 0;
  newValue = analogRead(_pin);
  if (newValue > _padCutOff)
  {
    if (!_active)
    {
      lastValue = 0;
      count = 0;
      while (count < 70)
      {
        if (newValue > lastValue)
        {
          lastValue = newValue;
          maxCount = count;
        }
        newValue = analogRead(_pin);
        // Serial.print(newValue);
        // Serial.print(",");
        count++;
      }
      //Serial.print("sensor max value = " );
      // Serial.print("\n");
      // Serial.print("Valor Maximo:");
      // Serial.print(lastValue);
      // Serial.print(", en sameple numero :");
      // Serial.println(maxCount);
      // delay(40 + lastValue * 3 / 100);
    
      // valor = (valor / 8) - 1;
      int valor = map(lastValue, 100, 4000, 0, 127);
      USBMIDI.sendNoteOn(midichannel, _padNote, valor);
      _playTime = 0;
      _active = true;
    }
    else
      _playTime++;
  }
  else if (_active)
  {
    _playTime++;
    if (_playTime > _maxPlayTime)
    {
      _active = false;
    }
  }
}


int hiHatPedal = PB5;

void HiHatPad::changeNote(int note)
{
  _padNote = note;
}

void HiHatPad::act()
{
  int hiHatPedalVel = 127;
  if(digitalRead(hiHatPedal))
  {
    if (!_hiHat)
    {
      _hiHat = true;
      changeNote(_basePadNote);   //hi hat opened
    }
  } else
  {   
    if (_hiHat)
    {
      _hiHat = false;
      USBMIDI.sendNoteOn(midichannel, _basePadNote, 0); //end note of open hi hat if playing
      if (this->_playTime < this->_maxPlayTime){
        USBMIDI.sendNoteOn(midichannel, _basePadNote + 3, 0); //if hi hat was open and playing, and hi hat pedal is suddenly on, hi hat choke
      }
      USBMIDI.sendNoteOn(midichannel, _basePadNote+2, hiHatPedalVel); //if hi hat was open, and hi hat pedal is suddenly on, hi hat pedal (45)
      changeNote(_basePadNote+1);   //hi hat closed
    }
  }
  basicAct();
}

Pad** arrayPads ; 
int maxPads = 8;



const int b1 = PA7; 
const int b2 = PA6; 
const int b3 = PA5; 
const int b4 = PA4; 
const int b5 = PA3; 
const int b6 = PA2; 
const int b7 = PA1; 
const int b8 = PA0; 

const int led = PC13;    
int arrayButtons[] = {b1, b2, b3, b4, b5, b6, b7, b8};


void setup()
{


  pinMode(hiHatPedal, INPUT_PULLUP);
  // digitalWrite(hiHatPedal, HIGH);
  USBMIDI.begin();

  // MIDI.begin();
  Serial.begin(115200);                                  // connect to the serial port 115200
  // arrayPads = new Pad*[6];
  // arrayPads[0] = new AnaloguePad(PA0, 110, 38);        // new HiHatPad(0, 550, 36, true);
  // arrayPads[1] = new AnaloguePad(PA1, 110, 39);
  // arrayPads[2] = new AnaloguePad(PA2, 120, 40);
  // arrayPads[3] = new AnaloguePad(PA3, 120, 41);
  // arrayPads[4] = new AnaloguePad(PA4, 120, 42);
  // arrayPads[5] = new HiHatPad(PA5, 120, 43);      //last one always, for pedal and open close

  arrayPads = new Pad*[8];
  int i=0;
  for (int pin : arrayButtons){
    pinMode(pin, INPUT_PULLUP);
    arrayPads[i] = new Pad(pin, (i*2)+38);
    i++;
  }
}

//*******************************************************************************************************************
// Main Program
//*******************************************************************************************************************

void loop()
{
  for (int pin = 0; pin < maxPads; pin++){
    arrayPads[pin]->act();
    }
}
