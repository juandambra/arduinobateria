#include <Arduino.h>


const int b1 = PA7; 
const int b2 = PA6; 
const int b3 = PA5; 
const int b4 = PA4; 
const int b5 = PA3; 
const int b6 = PA2; 
const int b7 = PA1; 
const int b8 = PA0; 

const int led = PC13;    

// These variables will change:
int newValue = 0;
int lastValue = 0;        // value read from the pot
int outputValue = 0;        // value output to the PWM
int count  = 0;
int maxCount = 0;
uint32 lastTime = 0;
int arrayButtons[] = {b1, b2, b3, b4, b5, b6, b7, b8};
int firstTime[8] = {0};
bool active[8] = {false};
void setup() {
    pinMode(led, OUTPUT);
    for (int pin : arrayButtons){
      pinMode(pin, INPUT_PULLUP);
    }
    // Configure LED pin
	Serial.begin(115200); // Ignored by Maple. But needed by boards using Hardware serial via a USB to Serial Adaptor
}

int maxWaitTime = 1000;
void loop() {
    // read the analog in value:
    int i = 0;
    int read = false;
    for (int pin : arrayButtons){
        int readValue = digitalRead(pin);
        if (!readValue){
            if (!active[i]){
                firstTime[i] = millis();
                Serial.print("Boton ");
                Serial.print(i+1);
                Serial.println(": ON");
                active[i] = true;
            }
            read = true;
            int now = millis();
            if (now - firstTime[i] > maxWaitTime){
                Serial.print("Boton ");
                Serial.print(i+1);
                Serial.println(": mantenido apretado");
            } 
        }
        else {
            active[i] = false;
        }
        i++;
    }
    if (read){
        digitalWrite(led, HIGH);
        delay(100);
        digitalWrite(led,LOW);
    }
    
    // newValue = analogRead(analogInPin);
    // // map it to the range of the analog out:
    // if (newValue > 120){
    //   lastValue = 0;
    //   count = 0;
    //   while (count < 70){
    //     if (newValue > lastValue){
    //       lastValue = newValue;
    //       maxCount = count;
    //     }
    //     newValue = analogRead(analogInPin);
    //     Serial.print(newValue);
    //     Serial.print(",");
    //     count++;     
    //   }
    //   //Serial.print("sensor max value = " );
    //   Serial.print("\n");
    //   Serial.print("Valor Maximo:");
    //   Serial.print(lastValue);
    //   Serial.print(", en sameple numero :");
    //   Serial.println(maxCount);
    //   delay(30 + lastValue*3/100);
    // }
    
}