#include <Arduino.h>


const int led = PC13; // Analog input pin that the potentiometer
                            // is attached to

void setup() {
  pinMode(led, OUTPUT);
	Serial.begin(115200); // Ignored by Maple. But needed by boards using Hardware serial via a USB to Serial Adaptor
}

void loop() {
  digitalWrite(led, HIGH);
  delay(1000);
  digitalWrite(led, LOW);
  delay(1000);
  Serial.println("Richard");  
}