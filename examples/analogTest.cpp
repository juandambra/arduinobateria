/*
  Analog input, analog output, serial output

  Reads an analog input pin, maps the result to a range from 0 to
  65535 and uses the result to set the pulse width modulation (PWM) of
  an output pin.  Also prints the results to the serial monitor.

  (You may need to change the pin numbers analogInPin and analogOutPin
  if you're not using a Maple).

  The circuit:
  * Potentiometer connected to analog pin 15.
    Center pin of the potentiometer goes to the analog pin.
    Side pins of the potentiometer go to +3.3V and ground.
  * LED connected from digital pin 9 to ground

  created 29 Dec. 2008
  by Tom Igoe

  ported to Maple
  by LeafLabs
*/

// These constants won't change.  They're used to give names
// to the pins used:

#include <Arduino.h>


const int analogInPin = PA2; // Analog input pin that the potentiometer
                            // is attached to

const int pwmOutPin = 9;    // PWM pin that the LED is attached to

// These variables will change:
int newValue = 0;
int lastValue = 0;        // value read from the pot
int outputValue = 0;        // value output to the PWM
int count  = 0;
int maxCount = 0;
void setup() {
    // Configure the ADC pin
    pinMode(analogInPin, INPUT_ANALOG);
    // Configure LED pin
	Serial.begin(115200); // Ignored by Maple. But needed by boards using Hardware serial via a USB to Serial Adaptor
}

void loop() {
    // read the analog in value:
    newValue = analogRead(analogInPin);
    // map it to the range of the analog out:
    if (newValue > 120){
      lastValue = 0;
      count = 0;
      while (count < 70){
        if (newValue > lastValue){
          lastValue = newValue;
          maxCount = count;
        }
        newValue = analogRead(analogInPin);
        Serial.print(newValue);
        Serial.print(",");
        count++;     
      }
      //Serial.print("sensor max value = " );
      Serial.print("\n");
      Serial.print("Valor Maximo:");
      Serial.print(lastValue);
      Serial.print(", en sameple numero :");
      Serial.println(maxCount);
      delay(30 + lastValue*3/100);
    }
    
}